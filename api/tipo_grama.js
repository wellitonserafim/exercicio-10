/*
    Lista apenas os pokemons do tipo grama (type = grass)
*/

var data = require('../data')

module.exports = function(req, res){
    
    var result = data.pokemon.filter((item) => item.type.includes("Grass"));
    
    //Retorno
    res.json(result)
}