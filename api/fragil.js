/*
    Lista apenas os pokemons com menos de 250 pontos de habilidade (total)
    A lista deve estar ordenada do menos habilidoso para o maior
*/

const { pokemon: pokemons } = require('../data')

module.exports = function (req, res) {

    const frageis = pokemons
        .filter((pokemon) => pokemon.total < 250)
        .sort((pokemonA, pokemonB) => pokemonA.total - pokemonB.total)

    res.json(frageis)
}