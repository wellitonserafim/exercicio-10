/*
    Lista apenas os pokemons do tipo agua (type = water)
*/

var data = require('../data')

module.exports = function(req, res){

    var result = data.pokemon.filter((item) => item.type.includes("Water"));
    
    //Retorno
    res.json(result)
}